<?php

namespace Pulsestorm\RepositoryTutorial\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Examples extends Command
{

    /*
     *хз чому не працює конструктор при php bin/magento setup:update
    */
//    protected $objectManager;
//    public function __construct(
//        \Magento\Framework\ObjectManagerInterface $objectManager,
//        \Magento\Framework\App\State $appState,
//        $name=null
//    ) {
//        $this->objectManager = $objectManager;
//        $appState->setAreaCode('frontend');
//        parent::__construct($name);
//    }

    protected function configure()
    {
        $this->setName("ps:examples");
        $this->setDescription("A command the programmer was too lazy to enter a description for.");
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->objectManager->get('Pulsestorm\ToDoCrud\Model\TodoItemRepository');
        $page = $repo->getById(2);
        echo get_class($page),"\n";
        $output->writeln("Hello World");
    }
}
